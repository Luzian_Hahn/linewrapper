#!/usr/bin/python3
import platform
from optparse import OptionParser
from linux import linuxEvaluation
from windows import windowsEvaluation
from correlationMethods import getCorrelationForData



parser = OptionParser()
parser.add_option("-i",
                  "--input",
                  dest="input",
                  type="string",
                  help="path of networkFile",
                  default="")
parser.add_option("-b",
                  "--binary",
                  dest="binary",
                  type="int",
                  help="choice for result in binary format",
                  default=0)
parser.add_option("-d",
                  "--dimension",
                  dest="dimension",
                  type="int",
                  help="half of result vector dimension",
                  default=50)
parser.add_option("-s",
                  "--samples",
                  dest="samples",
                  help="Number of samples (* 10^6) for EmbeddingAlgorithm",
                  default=10)
parser.add_option("-l",
                  "--learningRate",
                  dest="learningRate",
                  type="float",
                  help="learningRate for EmbeddingAlgorithm",
                  default=0.025)
parser.add_option("-t",
                  "--threads",
                  dest="threads",
                  type="int",
                  help="number of working threads",
                  default=2)
parser.add_option("-c",
                  "--correlation",
                  dest="correlation",
                  type=str,
                  help="path of file for correlation",
                  default="")

(opts, args) = parser.parse_args()



if __name__ == "__main__":
    if(platform.system() == 'Windows'):
        windowsEvaluation(opts, args)
    else:
        linuxEvaluation(opts, args)
    if opts.correlation != "":
        getCorrelationForData(opts, args)


