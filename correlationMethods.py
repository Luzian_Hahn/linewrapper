import pandas
import math
import numpy


def weightedCorrelation(rowsA, rowsB, weights=None, method="pearson"):

    myRowsA = rowsA.copy()
    myRowsB = rowsB.copy()

    if method == "spearman":
        myRowsA = pandas.Series(rowsA).rank()
        myRowsB = pandas.Series(rowsB).rank()

    covAB = weightedCov(myRowsA, myRowsB, weights)
    covAA = math.sqrt(weightedCov(myRowsA, myRowsA, weights))
    covBB = math.sqrt(weightedCov(myRowsB, myRowsB, weights))

    return covAB / (covAA * covBB)


def weightedMean(rows, weights=None):
    if weights is None:
        weights = numpy.ones(len(rows))
    if len(rows) != len(weights):
        raise ValueError("weight dimensions must match"+str(len(rows))+"____"+str(len(weights)))
    return rows.dot(weights) / weights.sum()


def weightedCov(rowsA, rowsB, weights=None):
    if len(rowsA) != len(rowsB):
        raise ValueError("row dimensions must match")
    if weights is None:
        weights = numpy.ones(len(rowsA))

    alignedRowsA = rowsA - weightedMean(rowsA, weights)
    alignedRowsB = rowsB - weightedMean(rowsB, weights)

    return sum([weights[i] * alignedRowsA[i] * alignedRowsB[i] for i in pandas.Series(alignedRowsA).index]) / weights.sum()

def getCorrelationForData(opts, args):
    weightFile = pandas.read_csv(opts.input, delimiter='\t', header=0, names=['termA', 'termB', 'weight'],
                                 dtype={'termA': str, 'termB': str, 'weight': float})
    correlFile = pandas.read_csv(opts.correlation, delimiter='\t', header=0,
                                 dtype={'termA': str, 'termB': str, 'relatedness': float})
    resultFile = pandas.read_csv('data/compared_edges_file', delimiter='\t', header=0,
                                 dtype={'termA': str, 'termB': str, 'relatedness': float})
    mergeFile = pandas.merge(weightFile, pandas.merge(correlFile, resultFile, how='inner', on=['termA', 'termB']),
                             how='inner', on=['termA', 'termB'])
    print('weighted Correlation: '
          +str(weightedCorrelation(mergeFile.relatedness_x, mergeFile.relatedness_y, weights=mergeFile.weight)))