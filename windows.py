import pickle
import pandas
import numpy
import math
import sys
import re
import subprocess

from ErrorCodes import *

def windowsEvaluation(opts, args):
    nodes = {}
    edges = []
    try:  # get Strings from network_file_path
        networkfile = open(opts.input, 'r')
        lines = networkfile.readlines()
        networkfile.close()
    except IOError:
        print("IOerror occured!")
        sys.exit(IOERROR_OPENING_INPUT_FILE)

    for line in lines:  # formatcheck network_file
        split = re.split(' |\t', line.strip())
        try:
            if split.__len__() != 3:
                print("Error1 in Format of network_file")
                sys.exit(FORMATERROR_IN_INPUT_FILE)
            float(split[2])
            edges += [(split[0], split[1])]
        except ValueError:
            print("Error2 in Format of network_file")
            sys.exit(FORMATERROR_IN_INPUT_FILE)

    try:  # execute LINE
        subprocess.call(['g++', '-lm', '-pthread', '-Ofast', '-march=native', '-Wall', '-funroll-loops',
                         '-ffast-math', '-Wno-unused-result', 'LINE-master/windows/line.cpp', '-o', 'line', '-lgsl',
                         '-lm', '-lgslcblas'])
        subprocess.call(['g++', '-lm', '-pthread', '-Ofast', '-march=native', '-Wall', '-funroll-loops',
                         '-ffast-math', '-Wno-unused-result', 'LINE-master/windows/reconstruct.cpp', '-o', 'reconstruct'])
        subprocess.call(['g++', '-lm', '-pthread', '-Ofast', '-march=native', '-Wall', '-funroll-loops',
                         '-ffast-math', '-Wno-unused-result', 'LINE-master/windows/normalize.cpp', '-o', 'normalize'])
        subprocess.call(['g++', '-lm', '-pthread', '-Ofast', '-march=native', '-Wall', '-funroll-loops',
                         '-ffast-math', '-Wno-unused-result', 'LINE-master/windows/concatenate.cpp', '-o', 'concatenate'])
        subprocess.call(
            ['reconstruct.exe', '-train', opts.input, '-output', 'tmp_network_dense_file', '-depth', '2', '-k-max',
             '1000'])
        subprocess.call(['line.exe', '-train', opts.input, '-output',
                         'tmp_vec1_wo_file', '-size', str(opts.dimension), '-binary', str(opts.binary), '-order', '1',
                         '-samples', str(opts.samples), '-threads', str(opts.threads), '-rho', str(opts.learningRate)])
        subprocess.call(['line.exe', '-train', opts.input, '-output',
                         'tmp_vec2_wo_file', '-size', str(opts.dimension), '-binary', str(opts.binary), '-order',
                         '2    ',
                         '-samples', str(opts.samples), '-threads', str(opts.threads), '-rho', str(opts.learningRate)])
        subprocess.call(
            ['normalize.exe', '-input', 'tmp_vec1_wo_file', '-output', 'tmp_vec1_file', 'binary', str(opts.binary)])
        subprocess.call(
            ['normalize.exe', '-input', 'tmp_vec2_wo_file', '-output', 'tmp_vec2_file', 'binary', str(opts.binary)])
        subprocess.call(['./concatenate', '-input1', 'tmp_vec1_file', '-input2', 'tmp_vec2_file', '-output',
                         'data/vec_complete_file', '-binary', str(opts.binary)])

        subprocess.call(['del', 'concatenate.exe', 'line.exe', 'reconstruct.exe', 'normalize.exe', 'tmp_vec1_file',
                         'tmp_vec2_file', 'tmp_vec1_wo_file', 'tmp_vec2_wo_file', 'tmp_network_dense_file'])
    except:
        print(sys.exc_info())
        sys.exit(EXECUTIONERROR_OF_LINE_PROGRAMM)

    try:  # fill dict with vector_values
        completeVectorFile = open('data/vec_complete_file', 'r')
        lines = completeVectorFile.readlines()
        vectors = pandas.read_csv('data/vec_complete_file', delimiter='\t', header=0)
        completeVectorFile.close()
    except IOError:
        print("IOerror occured!")
        sys.exit(IOERROR_OPENING_VECTOR_FILE)
    for indexLines in range(1, lines.__len__()):
        split = re.split(' |\t', lines[indexLines].strip())
        tmp = []
        for i in range(1, split.__len__()):
            tmp += [float(split[i])]
        #if (indexLines % 10 == 0):
            #print("Reading resulting Vector %.3f" % (100 * indexLines / lines.__len__()) + "%", end='\r', flush = True)
        nodes.update({split[0]: numpy.array(tmp)})
    
    print('\nSaving Vectors in pickle-Format')

    try:
        picklefile = open("data/LINE_result_pickle_format", 'w')
    except IOError:
        print("IOError occured!")
        sys.exit(IOERROR_WRITING_PICKLE_FILE)

    pickle.dump(vectors, picklefile)
    pickelfile.close()

    comparedEdgesFile = open('data/compared_edges_file', 'w')
    comparedEdgesFile.write("termA\ttermB\trelatedness\n")

    for i in range(0, edges.__len__()):
        (a, b) = edges[i]
        tmpVec1 = nodes.get(a)
        tmpVec2 = nodes.get(b)
        tmp = numpy.dot(tmpVec1, tmpVec2) / (math.sqrt(numpy.dot(tmpVec1, tmpVec1)) * (math.sqrt(numpy.dot(tmpVec2, tmpVec2))))
        comparedEdgesFile.write(a + '\t' + b + '\t' + str(tmp) + '\n')
        #if (i % 10 == 0):
            #print("Comparing  with cosine measure: %.3f" % (100 * i / edges.__len__()) + "%",end='\r', flush = True)
    comparedEdgesFile.close()
    print('\n')

    print("finished successfully")
